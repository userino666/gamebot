from aiogram.types import (ReplyKeyboardRemove, ReplyKeyboardMarkup, KeyboardButton, InlineKeyboardMarkup,
                           InlineKeyboardButton)

remove = ReplyKeyboardRemove()

main_menu_btn1 = KeyboardButton('🔍 Найти игру')
main_menu_btn2 = KeyboardButton('👤')
main_menu_btn3 = KeyboardButton('📊')
main_menu_btn4 = KeyboardButton('❓')
main_menu = ReplyKeyboardMarkup(resize_keyboard=True, row_width=3)
main_menu.row(main_menu_btn1)
main_menu.row(main_menu_btn2, main_menu_btn3, main_menu_btn4)

agree_btn = InlineKeyboardButton('Ознакомился', callback_data='agree')

game_ready_btn1 = InlineKeyboardButton('Принять', callback_data='game_ready')
game_ready_btn2 = InlineKeyboardButton('Отклонить', callback_data='game_reject')
game_ready = InlineKeyboardMarkup(row_width=2).add(game_ready_btn1, game_ready_btn2)
# game_ready

cancel_search_btn = InlineKeyboardButton('Отменить поиск', callback_data='cancel_search')
cancel_search = InlineKeyboardMarkup().add(cancel_search_btn)
# cancel_search.


action_btn1 = InlineKeyboardButton('✊🏻', callback_data='1')
action_btn2 = InlineKeyboardButton('✌🏻', callback_data='2')
action_btn3 = InlineKeyboardButton('✋🏻', callback_data='3')
action = InlineKeyboardMarkup(row_width=3).add(action_btn1, action_btn2, action_btn3)
# action

yes_no_btn1 = InlineKeyboardButton('Да', callback_data='yes')
yes_no_btn2 = InlineKeyboardButton('Нет', callback_data='no')
yes_no_menu = InlineKeyboardMarkup().add(yes_no_btn1, yes_no_btn2)
# report_btn = InlineKeyboardButton('⚠️Пожаловаться', callback_data='report')
after_battle_menu = InlineKeyboardMarkup(row_width=2).add(yes_no_btn1, yes_no_btn2)

deposit_btn1 = InlineKeyboardButton('20', callback_data='d20')
deposit_btn2 = InlineKeyboardButton('50', callback_data='d50')
deposit_btn3 = InlineKeyboardButton('100', callback_data='d100')
deposit_menu = InlineKeyboardMarkup(row_width=3).add(deposit_btn1, deposit_btn2, deposit_btn3)

profile_btn1 = InlineKeyboardButton('Банк', callback_data='bank')
profile_btn2 = InlineKeyboardButton('История игр', callback_data='game_history')
profile_btn3 = InlineKeyboardButton('Изменить имя', callback_data='change_name')
profile_btn4 = InlineKeyboardButton('Рефералы', callback_data='referrals')
profile_menu = InlineKeyboardMarkup(row_width=2).add(profile_btn1, profile_btn2, profile_btn3, profile_btn4)


bank_btn1 = InlineKeyboardButton('Пополнить BTC', callback_data='pay_btc')
bank_btn2 = InlineKeyboardButton('Пополнить QIWI', callback_data='pay_qiwi')
bank_btn3 = InlineKeyboardButton('Вывод', callback_data='withdrawal')
bank_btn4 = InlineKeyboardButton('История транзакций', callback_data='transaction_history')
bank_btn5 = InlineKeyboardButton('Обмен 🔆 ➡️ 💳 ', callback_data='changer')

bank_menu = InlineKeyboardMarkup(row_width=2).add(bank_btn1, bank_btn2, bank_btn3, bank_btn4, bank_btn5)
pay_menu = InlineKeyboardMarkup().add(bank_btn1, bank_btn2)


async def qiwi_url(comment):
    from utils import get_qiwi_url
    url = await get_qiwi_url(comment)
    qiwi_url_btn = InlineKeyboardButton('Перейти на сайт оплаты', url)
    return InlineKeyboardMarkup(row_width=1).add(qiwi_url_btn)


cancel_btn = ReplyKeyboardMarkup(resize_keyboard=True).add(KeyboardButton('❌Отменить'))

def pagination_menu(call_type):
    paginate_btn1 = InlineKeyboardButton('️⬅', callback_data=f'{call_type}_history_previous_page')
    paginate_btn2 = InlineKeyboardButton('️️➡', callback_data=f'{call_type}_history_next_page')
    back_btn = InlineKeyboardButton('🔙️Назад', callback_data=f'{call_type}_back')
    paginate = InlineKeyboardMarkup().add(paginate_btn1, paginate_btn2).row(back_btn)
    return paginate


async def choose_battle_options(max_money_bet, max_point_bet):
    MONEY_BETS = (5, 10, 20, 50, 100, 150, 300, 500, 1000, 5000)
    POINT_BETS = (20, 50, 100)
    BATTLE_ROUNDS_FOR_WIN = (3, 5, 7)
    bet_markup = InlineKeyboardMarkup()
    filtered_money_bets = tuple(filter(lambda x: x <= max_money_bet, MONEY_BETS))
    filtered_point_bets = tuple(filter(lambda x: x <= max_point_bet, POINT_BETS))
    quit_game = InlineKeyboardButton('❌ Покинуть', callback_data='quit_game')
    for_interest = InlineKeyboardButton('⚔️Просто бой', callback_data='0')
    # bet_markup.add(for_interest)
    # bet_markup.row()
    for bet in filtered_money_bets:
        bet_btn = InlineKeyboardButton(f'{bet} руб', callback_data=f'money_bet_{bet}')
        bet_markup.insert(bet_btn)
    bet_markup.row()
    for point_bet in filtered_point_bets:
        bet_btn = InlineKeyboardButton(f'{point_bet}🔆', callback_data=f'point_bet_{point_bet}')
        bet_markup.insert(bet_btn)
    bet_markup.row()
    for rounds in BATTLE_ROUNDS_FOR_WIN:
        rounds_btn = InlineKeyboardButton(f'{rounds}', callback_data=f'rounds_to_win_{rounds}')
        bet_markup.insert(rounds_btn)

    # bet_markup.add(bank_btn1, bank_btn1, bank_btn1, bank_btn1, bank_btn1)
    bet_markup.add(for_interest, quit_game)
    # bet_markup.add()
    return bet_markup


changer_btn1 = InlineKeyboardButton('🔆10000', callback_data='change_point_10k')
changer_btn2 = InlineKeyboardButton('🔆20000', callback_data='change_point_20k')
changer_btn3 = InlineKeyboardButton('🔆50000', callback_data='change_point_50k')

changer_menu = InlineKeyboardMarkup().add(changer_btn1, changer_btn2, changer_btn3)


help_btn1 = InlineKeyboardButton('Руководство', callback_data='help_instruction')
help_btn3 = InlineKeyboardButton('Правила', callback_data='help_rules')
help_btn2 = InlineKeyboardButton('Экономика', callback_data='help_economic')
# help_btn4 = InlineKeyboardButton('🔆10000', callback_data='help_')

help_menu = InlineKeyboardMarkup().add(help_btn1, help_btn2, help_btn3)
start_help_menu = InlineKeyboardMarkup().add(help_btn1, help_btn2, help_btn3, agree_btn)

back_btn = InlineKeyboardMarkup().add(InlineKeyboardButton('⬅️Назад', callback_data='help_back'))


report_btn1 = InlineKeyboardButton('Оскорбление', callback_data='report_for_1')
report_btn2 = InlineKeyboardButton('Отменить', callback_data='report_for_cancel')
report_menu = InlineKeyboardMarkup().add(report_btn1, report_btn2)

rating_btn1 = InlineKeyboardButton('🔆', callback_data='rating_points')
rating_btn2 = InlineKeyboardButton('📈', callback_data='rating_wr')
rating_menu = InlineKeyboardMarkup().add(rating_btn1, rating_btn2)