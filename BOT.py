import asyncio, pickle, logging
from aiogram.utils import executor
import utils
import qiwi
import keyboards as kb
from aiogram.dispatcher.webhook import get_new_configured_app
from aiogram.types import ContentTypes
from aiogram.utils.exceptions import MessageNotModified
from aiohttp import web
from antiflood import ThrottlingMiddleware
from config import *
from decorators import auth, get_user_or_auth
from messages import MSG, GAME_MSG, help_text, rules_text, economic_text, instruction_text, PLACES_EMOJI
from states import BotStates
from models import User, BattleResult, Transaction, Report

logging.basicConfig(level=logging.INFO)


@dp.message_handler(commands=['start'])
async def start_command(msg, state):
    try:
        User.get_by_id(msg.chat.id)
        return await bot.send_message(msg.chat.id, 'Добро пожаловать обратно', reply_markup=kb.main_menu)
    except User.DoesNotExist:
        ref_id = utils.get_ref(msg.text)
        await state.set_state(BotStates.START_INSTRUCTION)
        await state.set_data({'ref_id': ref_id})
        return await bot.send_message(msg.chat.id, help_text, reply_markup=kb.start_help_menu)


@dp.message_handler(commands=['kb'], state='*')
async def start_command(msg, state):
    state = await state.get_state()
    if state is None:
        await bot.send_message(msg.chat.id, MSG['main_menu'], reply_markup=kb.main_menu, disable_notification=True)
    else:
        await bot.send_message(msg.chat.id, '_Нет возможности получить клавиатуру_')


@dp.message_handler(regexp='Найти игру')
@auth
async def battle_deposit(msg, state, user):
    await bot.send_message(msg.chat.id, GAME_MSG['start_search_1'], reply_markup=kb.remove)
    sent_msg = await bot.send_message(msg.chat.id, GAME_MSG['start_search_2'], reply_markup=kb.cancel_search)
    await state.set_state(BotStates.SEARCH_BATTLE)
    return await game.create_player(msg.chat.id, sent_msg.message_id, user.name, user.balance, user.points, state)


@dp.callback_query_handler(lambda call: call.data in GAME_SEARCH_CALLBACKS, state=BotStates.SEARCH_BATTLE)
async def game_confirm(call, state):
    player = game.players.get(call.from_user.id, None)
    await bot.answer_callback_query(call.id)

    if player is None:
        return await bot.edit_message_text(chat_id=call.from_user.id, message_id=call.message.message_id,
                                           text='Что-то пошло не так, запустите поиск заново')

    if call.data == 'cancel_search':
        await game.delete_player(player, canceled=True)
    elif call.data == 'game_ready':
        await player.current_battle.start_check(player)
        await state.set_state(BotStates.CHOOSE_OPTIONS)
    elif call.data == 'game_reject':
        await state.reset_state()
        await bot.edit_message_text(chat_id=call.from_user.id, message_id=call.message.message_id,
                                    text=GAME_MSG['game_not_accepted'])
        await player.current_battle.destroy_battle(player)
        return await bot.send_message(player.user_id, text=MSG['main_menu'], reply_markup=kb.main_menu)


@dp.callback_query_handler(lambda call: True, state=BotStates.CHOOSE_OPTIONS)
async def get_user_bet_suggest(call, state):
    await bot.answer_callback_query(call.id)
    player = game.players.get(call.from_user.id, None)

    if call.data == 'quit_game':
        await bot.answer_callback_query(call.id, 'Вы покинули бой')
        await bot.edit_message_text(chat_id=player.user_id, message_id=call.message.message_id, text=GAME_MSG['leave'])
        await state.reset_state()
        await player.current_battle.destroy_battle(player)
        return await bot.send_message(player.user_id, text=MSG['main_menu'], reply_markup=kb.main_menu)
    elif call.data.startswith('rounds'):
        rounds_to_win = call.data[-1]
        await player.current_battle.get_options(player, rounds_to_win)
    else:
        bet_type = call.data[:5]
        bet = call.data[10:]
        await player.current_battle.get_options(player, bet=bet, bet_type=bet_type)

    player = game.players.get(call.from_user.id, None)


@dp.callback_query_handler(lambda call: True, state=BotStates.IN_BATTLE)
async def get_player_action(call):
    player = game.players.get(call.from_user.id, None)
    await bot.answer_callback_query(call.id)
    return await player.current_battle.player_action(player, call.data)


@dp.message_handler(commands=['report'], state=BotStates.IN_BATTLE)
async def report_player(msg, state):
    player = game.players[msg.chat.id]
    reason = msg.text.split(' ')[-1]
    Report.create(from_u=player.user_id, to_u=player.opponent.user_id, reason=reason)
    await msg.reply('Репорт отправлен')


@dp.message_handler(content_types=ContentTypes.TEXT, state=[BotStates.IN_BATTLE, BotStates.CHOOSE_OPTIONS,
                                                            BotStates.ASK_REVENGE])
async def battle_players_chat(msg):
    player = game.players.get(msg.chat.id, None)
    text = f'*{player.name}:* {msg.text}'
    if player.opponent is not None:
        player.current_battle.chat.append(text)
        return await bot.send_message(player.opponent.user_id, text)


@dp.callback_query_handler(lambda call: True, state=BotStates.ASK_REVENGE)
async def get_player_action(call, state):
    player = game.players[call.from_user.id]
    await bot.answer_callback_query(call.id)
    if call.data == 'yes':
        await state.set_state(BotStates.IN_BATTLE)
        await player.ask_revenge()
    else:
        await player.current_battle.no_revenge(player)


@dp.message_handler(regexp='📊')
async def statistic(message):
    return await bot.send_message(message.chat.id, MSG['statistic'] % (len(game.players), game.battles_quantity),
                                  reply_markup=kb.rating_menu)


@dp.callback_query_handler(lambda call: call.data.startswith('rating'))
async def rating(call):
    await bot.answer_callback_query(call.id)
    msg_text = call.message.text
    text = MSG['statistic'] % (len(game.players), game.battles_quantity)
    place = 0
    if call.data.endswith('points'):
        await bot.answer_callback_query(call.id)
        if '🔆' not in msg_text:
            text += MSG['points_rating']
            players = User.get_points_rating()
            for player in players:
                place += 1
                emoji = PLACES_EMOJI[place]
                text += f'\n{emoji}*{player.name}* - *{player.earned_points}*🔅'
    else:
        if '📈' not in msg_text:
            text += MSG['wr_rating']
            players = User.get_wins_rating()
            for player in players:
                place += 1
                emoji = PLACES_EMOJI[place]
                text += f'\n{emoji}*{player.name}* - *{player.games_won}*🏆 {player.winrate:.2f}%📈'
    try:
        await bot.edit_message_text(chat_id=call.from_user.id, message_id=call.message.message_id, text=text,
                                    reply_markup=kb.rating_menu)
    except MessageNotModified:
        pass


@dp.message_handler(regexp='👤')
@auth
async def profile(message, state, user):
    await state.set_data({'user': user})
    wr = '_вы еще не играли_' if user.winrate == 0.0 and user.games_q == 0 else f'{user.winrate:.2f}%'
    return await bot.send_message(message.chat.id, MSG['profile'].format(user.name, user.balance, user.points,
                                                                         user.games_q, user.games_won, user.games_lost,
                                                                         wr),
                                  reply_markup=kb.profile_menu)


@dp.callback_query_handler(lambda call: call.data == 'bank')
@get_user_or_auth
async def bank(call, state, user):
    await bot.answer_callback_query(call.id, 'Добро пожаловать в банк!')
    await bot.edit_message_text(chat_id=call.from_user.id, message_id=call.message.message_id,
                                text=MSG['bank'].format(user.balance, user.points),
                                reply_markup=kb.bank_menu)


@dp.callback_query_handler(lambda call: call.data in TRANSACTION_HISTORY_CALLBACKS)
@get_user_or_auth
async def player_transactions_history(call, state, user):
    data = await state.get_data()
    # Пагинация
    if call.data == 'transaction_back':
        await bot.answer_callback_query(call.id, '🏦')
        return await bot.edit_message_text(chat_id=call.from_user.id, message_id=call.message.message_id,
                                           text=MSG['bank'].format(user.balance, user.points),
                                           reply_markup=kb.bank_menu)
    elif call.data == 'transaction_history':
        page = 1
    else:
        page = await utils.get_page_number(data['page'], call.data)
        if not page:
            return await bot.answer_callback_query(call.id, 'Это всё.')

    transactions = user.transactions.where(Transaction.is_completed == True).paginate(page, 7)
    if not len(transactions):
        if page == 1:
            return await bot.answer_callback_query(call.id, 'Нет транзакций')
        return await bot.answer_callback_query(call.id, 'Это всё.')
    else:
        await state.update_data({'page': page})
        await bot.answer_callback_query(call.id, f'{page} страница')

    await bot.answer_callback_query(call.id, f'{page} страница')
    text = await utils.get_transactions_history_text(transactions)
    await bot.edit_message_text(chat_id=call.from_user.id, message_id=call.message.message_id, text=text,
                                reply_markup=kb.pagination_menu('transaction'))


@dp.callback_query_handler(lambda call: call.data == 'pay_btc' or call.data == 'pay_qiwi')
@get_user_or_auth
async def payment_details(call, state, user):
    await bot.answer_callback_query(call.id, 'Отправляем реквизиты')
    if call.data == 'pay_btc':
        wallet = user.btc_wallet
        currency = utils.btc_to_rub(1)
        await bot.send_message(call.from_user.id, text=MSG[call.data].format(currency))
        await bot.send_message(call.from_user.id, text=f'*{wallet}*')
    else:
        comment = user.qiwi_comment
        markup = await kb.qiwi_url(comment)
        await bot.send_message(call.from_user.id, text=MSG[call.data], reply_markup=markup)


@dp.callback_query_handler(lambda call: call.data == 'withdrawal')
async def payment_check(call, state):
    await state.set_state(BotStates.WITHDRAW_AMOUNT)
    await bot.send_message(call.from_user.id, MSG['withdrawal_get_amount'], reply_markup=kb.cancel_btn)


@dp.message_handler(regexp='Отменить', state=[BotStates.CHANGE_NAME, BotStates.WITHDRAW_AMOUNT,
                                              BotStates.WITHDRAW_WALLET, BotStates.CONFIRM_WITHDRAWAL])
async def back_to_main_menu(msg, state):
    await state.reset_state()
    return await bot.send_message(msg.chat.id, MSG['main_menu'], reply_markup=kb.main_menu, disable_notification=True)


@dp.message_handler(content_types=ContentTypes.TEXT, state=BotStates.WITHDRAW_AMOUNT)
@auth
async def withdrawal_get_wallet(msg, state, user):
    if await utils.validate_balance(user.balance, msg.text):
        await state.set_state(BotStates.WITHDRAW_WALLET)
        await state.update_data({'amount': msg.text})
        return await bot.send_message(msg.chat.id, MSG['withdrawal_get_wallet'])
    await bot.send_message(msg.chat.id, MSG['balance_validation_error'])


@dp.message_handler(content_types=ContentTypes.TEXT, state=BotStates.WITHDRAW_WALLET)
async def withdrawal_get_wallet(msg, state):
    data = await state.get_data()

    payment_system = await utils.validate_wallet_to_withdraw(msg.text)
    if payment_system:
        await state.set_state(BotStates.CONFIRM_WITHDRAWAL)
        await state.update_data({'payment_system': payment_system, 'wallet': msg.text})
        return await bot.send_message(msg.chat.id,
                                      MSG['confirm_withdrawal'].format(data['amount'], payment_system, msg.text),
                                      reply_markup=kb.yes_no_menu)
    else:
        await bot.send_message(msg.chat.id, MSG['wallet_validation_error'])


@dp.callback_query_handler(lambda call: call.data == 'yes' or call.data == 'no', state=BotStates.CONFIRM_WITHDRAWAL)
@get_user_or_auth
async def confirm_withdrawal(call, state, user):
    data = await state.get_data()
    if call.data == 'yes':
        if not user.withdrawal_unlock:
            return await bot.answer_callback_query(call.id, 'Вывод заблокирован')
        await bot.answer_callback_query(call.id, 'Создаем заявку')
        text = MSG['withdraw_created']
        withdraw_amount = data['amount']
        Transaction.create(user=user, type='withdraw', payment_system=data['payment_system'], payment_id=data['wallet'],
                           amount=withdraw_amount)
        user.decrease_balance(withdraw_amount)
        await bot.edit_message_reply_markup(chat_id=call.from_user.id, message_id=call.message.message_id)
        # Создаем заявку
    else:
        await bot.answer_callback_query(call.id)
        await bot.delete_message(chat_id=call.from_user.id, message_id=call.message.message_id)
        text = MSG['main_menu']
    await state.reset_state()
    await bot.send_message(call.from_user.id, text, reply_markup=kb.main_menu)


@dp.callback_query_handler(lambda call: call.data in GAME_HISTORY_CALLBACKS)
@get_user_or_auth
async def player_games_history(call, state, user):
    user_id = call.from_user.id
    data = await state.get_data()
    # Пагинация
    if call.data == 'game_back':
        await bot.answer_callback_query(call.id, '👤')
        wr = '_вы еще не играли_' if user.winrate == 0.0 and user.games_q == 0 else f'{user.winrate:.2f}%'
        return await bot.edit_message_text(chat_id=call.from_user.id, message_id=call.message.message_id,
                                           text=MSG['profile'].format(user.name, user.balance, user.points,
                                                                      user.games_q, user.games_won, user.games_lost, wr),
                                           reply_markup=kb.profile_menu)
    elif call.data == 'game_history':
        page = 1
    else:
        page = data.get('page', 1)
        page = await utils.get_page_number(page, call.data)
        if not page:
            return await bot.answer_callback_query(call.id, 'Это всё.')

    # загружаем батлы
    battles = BattleResult.user_last_battle_results(user_id, page)
    if not len(battles):
        if page == 1:
            return await bot.answer_callback_query(call.id, 'Вы еще не играли')
        return await bot.answer_callback_query(call.id, 'Это всё.')
    else:
        await state.update_data({'page': page})
        await bot.answer_callback_query(call.id, f'{page} страница')

    await bot.answer_callback_query(call.id, f'{page} страница')
    text = await utils.get_game_history_text(user_id, battles)
    await bot.edit_message_text(chat_id=call.from_user.id, message_id=call.message.message_id, text=text,
                                reply_markup=kb.pagination_menu('game'))


@dp.callback_query_handler(lambda call: call.data == 'change_name')
async def change_name(call, state):
    await bot.answer_callback_query(call.id)
    await bot.send_message(call.from_user.id, MSG['change_name'], reply_markup=kb.cancel_btn)
    await state.set_state(BotStates.CHANGE_NAME)


@dp.message_handler(content_types=ContentTypes.TEXT, state=BotStates.CHANGE_NAME)
@auth
async def process_change_name(msg, state, user):
    new_name = msg.text
    if await utils.validate_name(new_name):
        if not await utils.validate_balance(user.balance, change_name_cost):
            return await bot.send_message(msg.chat.id, MSG['balance_validation_error'], reply_markup=kb.pay_menu)
        user.update_name(new_name)
        user.decrease_balance(change_name_cost)
        await state.reset_state()
        await bot.send_message(msg.chat.id, MSG['name_changed'].format(msg.text), reply_markup=kb.main_menu)
    else:
        await bot.send_message(msg.chat.id, MSG['name_validation_error'].format(msg.text))


@dp.callback_query_handler(lambda call: call.data == 'referrals')
@get_user_or_auth
async def referrals(call, state, user):
    link = f'https://t.me/niawjdawbot?start={user.id}'
    await bot.answer_callback_query(call.id)
    await bot.send_message(call.from_user.id, MSG['referrals'].format(user.ref_quantity, link))


@dp.callback_query_handler(lambda call: call.data == 'changer')
async def bank_changer(call, state):
    await bot.answer_callback_query(call.id)
    await bot.send_message(call.from_user.id, MSG['bank_changer'], reply_markup=kb.changer_menu)


@dp.callback_query_handler(lambda call: call.data.startswith('change_point'))
@auth
async def change_point(call, state, user):
    points_amount = int(call.data[-3:-1]) * 1000
    if await utils.validate_balance(user.points, str(points_amount)):
        rub = points_amount // 100
        user.increase_balance(rub)
        user.decrease_points(points_amount)
        return await bot.answer_callback_query(call.id, MSG['points_changed'].format(rub), show_alert=True)
    await bot.answer_callback_query(call.id, MSG['balance_validation_error'])


@dp.message_handler(regexp='❓')
async def help_menu(msg):
    return await bot.send_message(msg.chat.id, help_text, reply_markup=kb.help_menu)


@dp.callback_query_handler(lambda call: call.data.startswith('help_'), state='*')
async def help_text_changer(call, state):
    state = await state.get_state()
    markup = kb.start_help_menu if state is not None else kb.help_menu
    await bot.answer_callback_query(call.id)
    if call.data.endswith('back'):
        return await bot.edit_message_text(chat_id=call.from_user.id, message_id=call.message.message_id, text=help_text,
                                           reply_markup=markup)
    elif call.data.endswith('instruction'):
        text = instruction_text
    elif call.data.endswith('rules'):
        text = rules_text
    elif call.data.endswith('economic'):
        text = economic_text
    await bot.edit_message_text(chat_id=call.from_user.id, message_id=call.message.message_id, text=text,
                                reply_markup=kb.back_btn)


@dp.message_handler(content_types=ContentTypes.TEXT, state=BotStates.CHOOSE_NAME)
async def get_new_user_name(msg, state):
    data = await state.get_data()
    if await utils.validate_name(msg.text):
        ref_id = data['ref_id']
        User.create_from_bot(msg, ref_id)
        await bot.send_message(msg.chat.id, MSG['registered'], reply_markup=kb.main_menu)
        if ref_id:
            await bot.send_message(ref_id, MSG['ref_registered'].format(msg.text))
        await state.reset_state()
    else:
        await bot.send_message(msg.chat.id, MSG['name_validation_error'].format(msg.text))


@dp.callback_query_handler(lambda call: call.data == 'agree', state=BotStates.START_INSTRUCTION)
async def process_choose_name(call, state):
    await bot.answer_callback_query(call.id, '👍🏻')
    await state.set_state(BotStates.CHOOSE_NAME)
    await bot.edit_message_text(chat_id=call.from_user.id, message_id=call.message.message_id, text=MSG['choose_name'])


async def on_startup(dp):
    await bot.get_webhook_info()

    with open('./other/battlers.pickle', 'rb') as f:
        players_list = pickle.load(f)
    for player in players_list:
        await bot.send_message(player, MSG['server_reload'], reply_markup=kb.main_menu)


async def on_shutdown(dp):
    await bot.delete_webhook()
    asyncio.get_event_loop().run_until_complete(bot.close())
    await bot.close()
    players_list = [p.user_id for p in game.players.values() if p.current_battle is not None]
    with open('./other/battlers.pickle', 'wb') as f:
        pickle.dump(players_list, f)

if __name__ == '__main__':
    dp.middleware.setup(ThrottlingMiddleware())
    # executor.start_polling(dp, on_shutdown=on_shutdown, on_startup=on_startup)

    app = get_new_configured_app(dispatcher=dp, path=WEBHOOK_PATH)
    app.add_routes([web.post('/qiwi_webhook', qiwi.qiwi_webhook)])

    app.on_startup.append(on_startup)
    app.on_shutdown.append(on_shutdown)

    web.run_app(app, host='localhost', port=443, ssl_context=SSL_CONTEXT)
