import asyncio

from aiogram.utils.exceptions import MessageNotModified

import keyboards as kb

from messages import GAME_MSG, MSG
from states import BotStates
# from models import BattleResult


class Battle:
    caster_message = 0
    target_message = 1

    def __init__(self, player1, player2, bot, game):
        self.players = (player1, player2)
        self.chat = []
        self.started = False
        self.bot = bot
        self.game = game
        self.round = 0
        self.deposit = 0  # реализовать добавление выбранного депозита
        self.deposit_type = None
        self.over = False
        self.bets_markup = None
        self.rounds_to_win = None
        self.revenge = False

        self.game.battles_quantity += 1
        self.afk_players = []

    async def start_check(self, player):
        player.ready = True
        if len(set(filter(lambda x: x.ready, self.players))) == 2:
            await self.send_deposits()
            # await self.start_battle()
        else:
            await self.bot.edit_message_text(chat_id=player.user_id, message_id=player.game_msg_id,
                                             text='Ожидаем противника')

    async def process_finish_battle(self, both_afk=None, winner=None, revenge=None):
        if both_afk:
            await self.destroy_battle()
        if winner and revenge:
            await self._save_battle(winner=winner)
            await self._clean_for_revenge()
            # очистить бой и игроков для новой игры
            for player in self.players:
                await player.state.set_state(BotStates.ASK_REVENGE)
                msg = await self.bot.send_message(player.user_id, GAME_MSG['ask_revenge'].format(player.opponent.name),
                                                  reply_markup=kb.after_battle_menu)
                player.game_msg_id = msg.message_id
            await self._revenge_timer()
        elif winner:
            await self._save_battle(winner=winner)
            await self.destroy_battle()

    async def destroy_battle(self, player=None):
        """Удаляет игроков и сам батл"""
        self.game.battles_quantity -= 1
        if player:
            player.current_battle = None
            await player.opponent.clean_battle()
            del self.game.players[player.user_id]
            del player  # удаляем экземпляр игрока
        else:
            for player in self.players:
                await self.bot.send_message(player.user_id, MSG['main_menu'], reply_markup=kb.main_menu,
                                            disable_notification=True)
                await player.state.reset_state()
                del self.game.players[player.user_id]
                del player
        del self

    async def send_deposits(self):
        max_money_bet = int(min([p.balance for p in self.players]))
        max_point_bet = int(min([p.points for p in self.players]))
        self.started = True
        self.bets_markup = await kb.choose_battle_options(max_money_bet=max_money_bet, max_point_bet=max_point_bet)
        for player in self.players:
            await player.state.set_state(BotStates.CHOOSE_OPTIONS)
            await self.bot.edit_message_text(chat_id=player.user_id, message_id=player.game_msg_id,
                                             text=GAME_MSG['game_started'].format(player.opponent.name))
            op = player.opponent
            msg = await self.bot.send_message(player.user_id, GAME_MSG['game_bet_info'].format(player.bet, '',
                                                                                               player.rounds_to_win,
                                                                                               op.name, op.bet, '',
                                                                                               op.rounds_to_win),
                                              reply_markup=self.bets_markup)
            player.game_msg_id = msg.message_id

    async def get_options(self, player, rounds_to_win=None, bet=None, bet_type=None):
        if rounds_to_win:
            player.rounds_to_win = rounds_to_win
        else:
            emoji = '🔆' if bet_type == 'point' else 'руб'
            if bet_type == '0':
                bet = '⚔️'
                player.bet = bet
                player.emoji_bet = ''
            else:
                player.bet = bet
                player.emoji_bet = emoji
        await self._check_options(player)

    async def _check_options(self, player):
        op = player.opponent
        if player.bet == op.bet and player.emoji_bet == op.emoji_bet and player.rounds_to_win != '❔'\
                and player.bet != '❔'\
                and player.rounds_to_win == op.rounds_to_win:
            try:
                self.deposit = int(player.bet)
            except ValueError:
                pass
            self.rounds_to_win = int(player.rounds_to_win)
            self.deposit_type = player.emoji_bet
            for player in self.players:
                    await self.bot.edit_message_text(chat_id=player.user_id, message_id=player.game_msg_id,
                                                     text=GAME_MSG['bet_chosen'].format(player.bet, player.emoji_bet,
                                                                                        player.rounds_to_win))
                    await player.state.set_state(BotStates.IN_BATTLE)
            return await self.send_round(new_battle=True)
        else:
            for player in self.players:
                op = player.opponent
                text = GAME_MSG['game_bet_info'].format(player.bet, player.emoji_bet, player.rounds_to_win,
                                                        op.name, op.bet, op.emoji_bet, op.rounds_to_win)
                try:
                    await self.bot.edit_message_text(chat_id=player.user_id, message_id=player.game_msg_id, text=text,
                                                 reply_markup=self.bets_markup)
                except MessageNotModified:
                    pass

    async def player_action(self, player, action_id):
        player.action = int(action_id)
        opponent = player.opponent

        if opponent.action is None:
            text = GAME_MSG['choose_action'].format(self.round, player.name, player.action_emoji, opponent.name,
                                                    '❔')
            return await self.bot.edit_message_text(chat_id=player.user_id, message_id=player.game_msg_id,
                                                    text=text)
        else:
            await self._send_round_result(player, opponent)

    async def _send_round_result(self, player, opponent):
        await self._round_result(player, opponent)

        for player in self.players:
            opponent = player.opponent
            text = GAME_MSG['round_result'].format(self.round, player.name, player.action_emoji, player.rounds_won,
                                                   opponent.name, opponent.action_emoji, opponent.rounds_won)
            if player.round_win is None:
                text += GAME_MSG['round_draw']
            elif player.round_win:
                text += GAME_MSG['round_won']
            else:
                text += GAME_MSG['round_lose']

            await self.bot.edit_message_text(chat_id=player.user_id, message_id=player.game_msg_id, text=text)

        if player.rounds_won == self.rounds_to_win:
            return await self._end_game(player)
        elif opponent.rounds_won == self.rounds_to_win:
            return await self._end_game(opponent)

        await self.send_round()

    async def send_round(self, new_battle=None):
        self.round = 1 if new_battle else self.round + 1
        for player in self.players:
            player.action = None
            text = GAME_MSG['choose_action'].format(self.round, player.name, '❔', player.opponent.name, '❔') \
                   + '\n\n_Выберите действие:_'
            msg = await self.bot.send_message(chat_id=player.user_id, text=text, parse_mode='Markdown',
                                              reply_markup=kb.action)
            player.game_msg_id = msg.message_id
        await self.afk_timer()

    @staticmethod
    async def _round_result(player, opponent):
        action1 = player.action
        action2 = opponent.action
        if action1 == 1 and action2 == 2:
            await player.won()
        elif action1 == 2 and action2 == 3:
            await player.won()
        elif action1 == 3 and action2 == 1:
            await player.won()
        # won
        elif action1 == 2 and action2 == 1:
            await opponent.won()
        elif action1 == 3 and action2 == 2:
            await opponent.won()
        elif action1 == 1 and action2 == 3:
            await opponent.won()
        # draw
        elif action1 == action2:
            player.round_win = None
            opponent.round_win = None

    async def _end_game(self, winner, afk_lose=False):
        self.over = True
        win_text = GAME_MSG['game_won']
        lose_text = GAME_MSG['game_lose']
        if afk_lose:
            win_text += '\n_Техническая победа противник бездействовал слишком долго_'
            lose_text += '\n_Техническое поражение вы бездействовали слишком долго_'

        await self.bot.send_message(winner.user_id, win_text)
        await self.bot.send_message(winner.opponent.user_id, lose_text)
        if winner.rounds_won == self.rounds_to_win:
            return await self.process_finish_battle(winner=winner, revenge=True)
        return await self.process_finish_battle(winner=winner)

    async def accept_timer(self):
        await asyncio.sleep(15)
        if self.players[0].current_battle is not None and not self.started:
            for player in self.players:
                if not player.ready:
                    await self.game.delete_player(player)
                else:
                    await player.clean_battle()

    async def _revenge_timer(self):
        await asyncio.sleep(5)
        if not self.revenge:
            player = list(filter(lambda x: not x.revenge, self.players))[0]
            return await self.no_revenge(player)

    async def no_revenge(self, player):
        self.revenge = False

        await self.bot.delete_message(chat_id=player.user_id, message_id=player.game_msg_id)
        await self.bot.edit_message_text(chat_id=player.opponent.user_id,
                                         message_id=player.opponent.game_msg_id,
                                         text=GAME_MSG['no_revenge'])
        await self.destroy_battle()

    async def afk_timer(self):
        self.afk_players.clear()
        last_round = self.round
        await asyncio.sleep(10)
        is_round_not_changed = last_round == self.round and not self.over

        if is_round_not_changed:
            for player in self.players:
                if not player.action:
                    await self.bot.send_message(player.user_id, GAME_MSG['afk_warning'])
                    self.afk_players.append(player)
                    # await self.send_afk_warning(player)

        await asyncio.sleep(5)
        if is_round_not_changed:
            update_afk_players = list(filter(lambda p: p.action is None, self.afk_players))
            if len(update_afk_players) == 2:
                return await self._both_afk()
            try:
                looser = update_afk_players[0]
            except IndexError:
                return
            await self._end_game(winner=looser.opponent, afk_lose=True)

    async def _both_afk(self):
        for player in self.players:
            await self.bot.edit_message_text(chat_id=player.user_id, message_id=player.game_msg_id, text=GAME_MSG['both_afk'])
        await self.process_finish_battle(both_afk=True)

    async def _save_battle(self, winner):
        from models import BattleResult, User

        BattleResult.create_from_bot(winner_id=winner.user_id, loser_id=winner.opponent.user_id,
                                     deposit=self.deposit, deposit_type=self.deposit_type, chat='\n'.join(self.chat))

        winner_user = User.get_by_id(winner.user_id)
        loser_user = User.get_by_id(winner.opponent.user_id)
        save = True if self.deposit_type == '' else False
        User.calc_stats({'winner': winner_user, 'loser': loser_user}, save=save)

        if self.deposit_type == 'руб':
            amount = self.deposit * 0.9  # 90% от выиграша
            winner_user.increase_balance(amount)
            loser_user.decrease_balance(self.deposit)
        elif self.deposit_type == '🔆':
            winner_user.increase_points(self.deposit)
            loser_user.decrease_points(self.deposit)

    async def _clean_for_revenge(self):
        self.round = 0
        self.over = False
        self.deposit = 0
        self.deposit_type = None
        self.rounds_to_win = None
        self.revenge = False
