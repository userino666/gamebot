import re
import uuid

import requests

import keyboards
from config import node
# from models import BattleResult


def get_ref(text):
    try:
        referral = text.split(' ')[1]
    except IndexError:
        return
    return referral


def create_new_btc_wallet():
    return node.getnewaddress()


def create_qiwi_id():
    return uuid.uuid4()


async def get_qiwi_url(comment):
    phone = '3806666666'
    link = f'https://qiwi.com/payment/form/99?extra%5B%27account%27%5D={phone}&extra%5B%27comment%27%5D={comment}'
    return link


async def validate_name(name):
    return re.match('^[A-Za-z0-9_-]*$', name) and 3 <= len(name) <= 15


async def get_game_history_text(user_id, battles):
    text = '*История игр:*\n\n'
    for battle in battles:
        cash = '' if not battle.cash else battle.cash
        if battle.winner == user_id:
            emoji = '🏆'
            result = 'победа'
            transaction_sign = '➕'
        else:
            emoji = '🏴'
            result = 'поражение'
            transaction_sign = '➖'
        if battle.deposit_type == '':
            transaction_sign = '⚔️'

        text += f'{emoji}Игра: _id {battle.id}_\nРезультат: {result} {transaction_sign} *{cash} ' \
                f'{battle.deposit_type}*\n\n'
    return text


async def get_transactions_history_text(transactions):
    text = '*История транзакций:*\n\n'
    for t in transactions:
        emoji = '➕' if t.type == 'refill' else '➖'
        system = 'BTC' if t.payment_system == 'btc' else 'QIWI'
        data = str(t.created_at)[:-7]
        text += f'{emoji} _ID: {t.id}_\n💸Сумма: {t.amount} {system}\n🕒Дата: {data}\n\n'
    return text


async def get_page_number(current_page, call_data):
    if call_data.endswith('history'):
        return 1

    if call_data.endswith('next_page'):
        current_page += 1
    else:
        if current_page == 1:
            return
        current_page -= 1
    return current_page


async def validate_balance(balance, amount):
    if amount.isdigit() and balance >= int(amount):
        return True


async def validate_wallet_to_withdraw(wallet):
    print('380630776161')
    if wallet.isdigit() and len(wallet) == 12 or len(wallet) == 11:
        print('qiwi')
        return 'QIWI'
    elif wallet.isalnum() and len(wallet) == 34:
        print('btc')
        return 'BTC'


def btc_to_rub(amount):
    btc_in_usd = requests.get('https://api.bitaps.com/market/v1/ticker/btcusd').json()['data']['last']
    usd_to_rub = requests.get('https://api.exchangeratesapi.io/latest?base=USD').json()['rates']['RUB']
    btc_in_rub = int(btc_in_usd * usd_to_rub)
    rub_amount = int(btc_in_rub * amount)
    return rub_amount


async def user_not_found_notify(user_id, call_id):
    from config import bot
    if call_id:
        await bot.answer_callback_query(call_id, 'Упс 😞')
    return await bot.send_message(user_id, 'Вероятно ваш аккаунт был удален или заблокирован!',
                                  reply_markup=keyboards.remove)
