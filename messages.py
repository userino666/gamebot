bet_chosen = '''
💰Утвержденная ставка: *{}* {}
◾️ Кол-во раундов: *{}*

_Не забудьте сообщить о скверном поведении противника, для этого напишите "/report причина"_
'''

GAME_MSG = {
    'deposit': '💳 Депозит: *{}* руб\n\n',
    'start_search_1': '🔎 *Поиск запущен*', # 💳 Депозит: *{}* руб\n\n
    'start_search_2': '_Ожидайте противника..._',
    'stop_search': 'Поиск был отменен',
    'game_not_accepted': 'Вы не приняли игру. Поиск отменен',
    'game_started': 'Игра началась\nВаш противник: *{}*\n\n_Договоритесь о ставке_',
    'game_bet_info': 'Ваша ставка: *{}* {}       _раунды:_ *{}*\n{}: *{}* {}       _раунды:_ *{}*',
    'bet_chosen': bet_chosen,
    'leave': 'Вы покинули текущий бой',

    'round_result': '*Раунд {}*\n\n{}: {}        *{}*🏆\n{}: {}        *{}*🏆',
    'round_lose': '\n\n_Вы проиграли этот раунд_',
    'round_won': '\n\n_Этот раунд ваш_',
    'round_draw': '\n\n_Ничья_',

    'game_lose': '🏴*Вы проиграли!*',
    'game_won': '🏆*Вы победили!*',
    'ask_revenge': '\n\n_Предложить_ *{}* _сыграть еще раз?_',
    'no_revenge': 'Противник отказался от участия',

    'choose_action': '*Раунд {}*\n\n{}: {}\n{}: {}',
    'opponent_not_accepted': 'Противник отклонил игру. Поиск продолжится',

    'afk_warning': 'Осталость 5 сек на ответ',
    'both_afk': 'Оба участника афк. Бой завершен без  сохранение результата',
    'report': 'Укажите причину жалобы',
}

pay_btc = '''
💳*Пополнить BTC*

Чтобы пополнить ваш баланс просто отправьте любое количество BTC на ваш персональный адрес для пополнения (след. сообщение)

Деньги будут автоматически зачислены на баланс по курсу BTC на момент зачисления.
Время ожидания: до *10 мин*

_Текущий курс:
1 BTC = {} руб_
'''
pay_qiwi = '''
💳*Пополнить QIWI*

Чтобы пополнить ваш баланс просто перейдите по ссылке и введите сумму пополнения.
_Не изменяйте номер кошелька и коментарий!_

Деньги будут автоматически зачислены на баланс.
Время ожидания: до *10 мин*
'''
choose_name = '''
✏️ *Введите свой никнейм*

_Используется в сражениях и чате_
*Латинские буквы и цифры от 3 до 10 символов*
'''
profile = '''
👤Профиль: *{}*

💳 Баланс: *{} руб*
🔆 Баллы: *{}*

📊*Статистика*
_Кол-во игр:_ *{}*
_Побед:_ *{}*
_Поражений:_ *{}*
_WinRate:_ *{}*
'''
# 🎮🏆🏴📈
MSG = {
    'main_menu': 'Главное меню',
    'choose_name': choose_name,
    'statistic': '👥 Игроков онлайн: %s\n🎮 Сражений онлайн: %s',
    'registered': 'Вы успешно зарегистрировались',
    'choose_deposit': 'Выберите депозит',
    'profile': profile,
    'bank': '🏦*Банк*\n\n_Совершайте операции с балансом и просматривайте историю транзакций_\n\n💳 Баланс: *{} руб*\n🔆 Баллы: *{}*',
    'pay_btc': pay_btc,
    'pay_qiwi': pay_qiwi,
    'change_name': 'Отправьте новое имя:\n\n_Изменение имени стоит 100 рублей_',
    'name_changed': 'Новое имя: *{}*',
    'name_validation_error': 'Ошибка валидации имени',
    'balance_validation_error': 'Не достаточно средств',
    'wallet_validation_error': 'Ошибка валидации счет для вывода',
    'referrals': 'Вы пригласили *{}* пользователей\n\n_Ваша реферальная ссылка:_\n{}',
    'withdrawal_get_amount': '*Введите сумму для вывода*',
    'withdrawal_get_wallet': '*Введите кошелек для вывода*',
    'confirm_withdrawal': '*Потдвердите заявку*\n\nСумма: {}\nТип: {}\nКошелек: {}',
    'withdraw_created': '*Заявка создана*',
    'ref_registered': '*Новый реферал* ➕500🔆\n_По вашей ссылке зарегистрировался_ *{}*',
    'bank_changer': '🔁*Обмен*\n\nВыберите кол-во баллов которые хотите обменять:',
    'points_changed': 'Вы успешно обменяли баллы.\nБаланс пополнен ➕ {}',
    'server_reload': 'Сервер был перезагружен. Результат боя сохранить не удалось 😔',
    'points_rating': '\n\n🔆*Рейтинг игроков*\n_по кол-ву заработанных баллов_\n',
    'wr_rating': '\n\n📈*Рейтинг игроков*\n_по кол-ву выигранных игр_\n',
    'profile_stats': '\n\n📈*Рейтинг игроков*\n_по кол-ву выигранных игр_\n',
}

help_text = '''
Всеми любимая игра нашего детства *Камень, ножницы, бумага* в вашем телефоне.
Тут Вам предстоит сыграть не с подлым ботом, а с настоящим живым противником!

_Приглашайте друзей и получайте бонусные баллы_😉
'''
instruction_text = '''
1️⃣Нажимаете *"Найти игру"* и ожидаете противника.

2️⃣Как только игра будет готова, у Вас будет *15 секунд* чтобы принять её, иначе вас просто выбросит из поиска.

3️⃣С этого момента вам доступен чат с противником. Просто напишите текстовое сообщение боту.
Не забудьте ознакомится с *"Правила"*. В этот момент вы можете покинуть игру без последствий.

4️⃣Дальше Вам предстоит выбрать ставку за которую вы будете сражаться (подробней о финансах в разделе *"Экономика"*)
_в клавиатуре выбора ставки присутствуют все доступные ставки на текущий бой_

5️⃣Как только Ваша ставка совпадет со ставкой противника - начнется раунд. Всего игра длится до *трёх* побед,
кто первый выиграет три раунда тот и победил!

6️⃣Каждый раунд у вас будет *30 секунд* чтобы выбрать действие, если не было действия от обоих участников - игра 
засчитана не будет, если от одного - он получит *техническое поражение*.
'''
economic_text = '''
В игре можно играть *бесплатно*, на *баллы* или на *деньги*.

*"Бесплатно"* это игра на интерес, вы можете решить спор со своим другом или просто скоротать время в онлайн сражении.

*Баллы* игровая финансовая единица для поощерения активных пользователей. Заработать баллы можно с помощью реферальной \
системы, за каждого реферала вы получите *500 баллов*. Баллы можно обменять на деньги в *банке*

*Деньги* это игра с противником за реальный *финансовый приз*. Тот кто одержал победу получит ставку которую противники определили в начале боя. _Комиссия 10%_
'''
# *👤➡️Рефералы*  *👤➡Банк➡Обмен*
rules_text = '''
А правила очень просты 🤓
В чате нельзя оскорблять противника
'''

PLACES_EMOJI = {
    1: '🥇',
    2: '🥈',
    3: '🥉',
    4: '4️⃣',
    5: '5️⃣',
    6: '6️⃣',
    7: '7️⃣',
    8: '8️⃣',
    9: '9️⃣',
    10: '1️⃣0️⃣',
    11: '1️⃣1️⃣',
    12: '1️⃣2️⃣',
    13: '1️⃣3️⃣',
    14: '1️⃣4️⃣',
    15: '1️⃣5️⃣',
    16: '1️⃣6️⃣',
    17: '1️⃣7️⃣',
    18: '1️⃣8️⃣',
    19: '1️⃣9️⃣',
    20: '2️⃣0️⃣',
    21: '2️⃣1️⃣',
    22: '2️⃣2️⃣',
    23: '2️⃣3️⃣',
    24: '2️⃣4️⃣',
    25: '2️⃣5️⃣',
}
