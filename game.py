from collections import OrderedDict

from messages import GAME_MSG, MSG
from battle import Battle
from player import Player
import keyboards as kb


class Game:
    players = OrderedDict()
    battles_quantity = 0

    def __init__(self, bot):
        self.bot = bot

    async def create_player(self, user_id, msg_id, name, balance, points, state):
        player = Player(user_id, msg_id, self.bot, name, balance, points, state)
        await self._add_player(player)

    async def _add_player(self, player):
        self.players[player.user_id] = player
        await self._search_opponent(player)  # запускаем поиск противника

    async def _search_opponent(self, player):
        available_players = [p for p in self.players.values() if p.user_id != player.user_id
                             and p.current_battle is None]

        if not len(available_players):
            return

        opponent = available_players[0]
        await self._create_battle(player, opponent)

    async def _create_battle(self, player, opponent):
        battle = Battle(player, opponent, self.bot, self)
        return await self._get_players_ready(battle)

    async def _get_players_ready(self, battle):
        for player in battle.players:
            player.current_battle = battle
            await player.get_opponent()
            await self.bot.delete_message(player.user_id, player.game_msg_id)
            msg = await self.bot.send_message(chat_id=player.user_id, text='*🎮 Игра готова*', reply_markup=kb.game_ready)
            player.game_msg_id = msg.message_id
        await battle.accept_timer()

    async def delete_player(self, player, canceled=None):
        text = GAME_MSG['stop_search'] if canceled else GAME_MSG['game_not_accepted']
        await player.state.reset_state()
        await self.bot.edit_message_text(chat_id=player.user_id, message_id=player.game_msg_id, text=text)
        await self.bot.send_message(player.user_id, MSG['main_menu'], reply_markup=kb.main_menu)
        del self.players[player.user_id]
