import hashlib
import hmac
from aiohttp import web
from config import webhook_key
from models import User, Transaction


async def get_private_hash(data):
    data = data.replace(',', '|')
    private_hash = hmac.new(webhook_key, data.encode('utf-8'), hashlib.sha256).hexdigest()
    return private_hash


async def qiwi_webhook(request):
    data = await request.post()

    hash = data['hash']
    payment = data['payment']
    sign_fields = payment['signFields']
    person_id = payment['personId']
    comment = payment['comment']
    amount = payment['total']['amount']

    # check payment hash
    private_hash = await get_private_hash(sign_fields)
    if hash != private_hash:
        return web.HTTPForbidden()

    try:
        user = User.select().where(User.qiwi_comment == comment)
    except User.DoesNotExist:
        return web.HTTPForbidden()

    Transaction.create(user=user, type='refill', payment_system='qiwi', payment_id=person_id,
                       amount=amount, is_completed=True)
    user.increase_balance(amount)
    return web.HTTPOk()