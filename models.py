import datetime

from peewee import *

import utils

db = PostgresqlDatabase('kbn', user='kbn_u', password='GrD60u1q1O6PI70', host='localhost')


class BaseModel(Model):
    class Meta:
        database = db


class User(BaseModel):
    id = IntegerField(primary_key=True)
    username = CharField(max_length=50, null=True)
    name = CharField(max_length=15)
    registered_at = DateTimeField(default=datetime.datetime.now())
    referral = IntegerField(null=True)
    ref_quantity = IntegerField(default=0)

    balance = IntegerField(default=0)
    points = IntegerField(default=0)
    earned_points = IntegerField(default=0)
    btc_wallet = CharField(max_length=34)
    qiwi_comment = CharField(max_length=36)
    withdrawal_unlock = BooleanField(default=False)

    winrate = FloatField(default=0)
    games_won = IntegerField(default=0)
    games_lost = IntegerField(default=0)

    @classmethod
    def create_from_bot(cls, message, ref_id):
        user_id = message.chat.id
        username = message.from_user.username
        name = message.text
        btc_wallet = utils.create_new_btc_wallet()
        qiwi_comment = utils.create_qiwi_id()
        if ref_id:
            try:
                cls.get_by_id(ref_id).ref_registered()
            except DoesNotExist:
                pass
        return cls.create(id=user_id, username=username, name=name, referral=ref_id, btc_wallet=btc_wallet,
                          qiwi_comment=qiwi_comment)

    def update_name(self, new_name):
        self.name = new_name
        self.save()

    def increase_balance(self, amount):
        self.balance += int(amount)
        self.save()

    def decrease_balance(self, amount):
        self.balance -= int(amount)
        self.save()

    def increase_points(self, amount):
        self.earned_points += int(amount)
        self.points += int(amount)
        self.save()

    def decrease_points(self, amount):
        self.points -= int(amount)
        self.save()

    @staticmethod
    def calc_stats(players, save=None):
        for i, player in players.items():
            winner = True if i == 'winner' else False
            player._calc_stats(save, winner)

    def _calc_stats(self, save, winner):
        if winner:
            self.games_won += 1
        else:
            self.games_lost += 1

        winrate = self.games_won * 100 / self.games_q
        self.winrate = winrate
        if save:
            self.save()

    @classmethod
    def get_points_rating(cls):
        return cls.select(cls.name, cls.earned_points).order_by(-cls.earned_points)[:25]

    @classmethod
    def get_wins_rating(cls):
        return cls.select(cls.name, cls.games_won, cls.winrate).order_by(-cls.games_won, -cls.winrate)[:25]

    def ref_registered(self):
        self.points += 500
        self.earned_points += 500
        self.ref_quantity += 1
        self.save()

    @property
    def games_q(self):
        return self.games_lost + self.games_won


class BattleResult(BaseModel):
    id = PrimaryKeyField()
    winner = IntegerField()
    loser = IntegerField()
    cash = IntegerField()
    deposit_type = CharField(max_length=5, null=True)
    chat = TextField(null=True)

    @classmethod
    def create_from_bot(cls, winner_id, loser_id, deposit, deposit_type, chat):
        cls.create(winner=winner_id, loser=loser_id, cash=deposit, deposit_type=deposit_type, chat=chat)

    @classmethod
    def user_last_battle_results(cls, user_id, page):
        return cls.select().where((cls.winner == user_id) | (cls.loser == user_id)).order_by(-cls.id).paginate(page, 7)


class Transaction(BaseModel):
    TRANSACTION_TYPES = (
        ('withdraw', 'Вывод'),
        ('refill', 'Пополнение')
    )
    PAYMENTS_SYSTEMS = (
        ('btc', 'BTC'),
        ('qiwi', 'QIWI'),
    )

    user = ForeignKeyField(User, backref='transactions')
    type = CharField(max_length=8, choices=TRANSACTION_TYPES)
    payment_system = CharField(max_length=4, choices=PAYMENTS_SYSTEMS)
    payment_id = CharField(max_length=36)
    amount = IntegerField()

    created_at = DateTimeField(default=datetime.datetime.now())
    is_completed = BooleanField(default=False)

    @classmethod
    def get_user_transactions(cls):
        cls.select().join(User)


class Report(BaseModel):
    from_u = IntegerField()
    to_u = IntegerField()
    reason = CharField(max_length=100)
