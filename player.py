from messages import GAME_MSG
import keyboards as kb
from states import BotStates


class Player:
    def __init__(self, user_id, msg_id, bot, name, balance, points, state):
        self.user_id = user_id
        self.ready = False
        self.current_battle = None
        self.game_msg_id = msg_id
        self.bot = bot
        self.action = None
        self.round_win = None
        self.rounds_won = 0
        self.name = name
        self.state = state
        self.revenge = None
        self.in_search = True
        self.bet = '❔'
        self.opponent = None
        self.balance = balance
        self.points = points
        self.emoji_bet = ''
        self.rounds_to_win = '❔'

    async def clean_battle(self, revenge=False):
        text = GAME_MSG['no_revenge'] if revenge else GAME_MSG['opponent_not_accepted']
        if revenge:
            op = self.opponent
            await op.state.reset_state()
            await self.bot.edit_message_text(chat_id=op.user_id, message_id=op.game_msg_id,
                                             text=text)
        else:
            await self.state.set_state(BotStates.SEARCH_BATTLE)
            self.in_search = True
            await self.bot.edit_message_text(chat_id=self.user_id, message_id=self.game_msg_id,
                                             text=text, reply_markup=kb.cancel_search)
        self.current_battle = None
        self.ready = False
        self.opponent = None

    @property
    def action_emoji(self):
        if self.action == 1:
            return '✊🏻'
        elif self.action == 2:
            return '✌🏻'
        elif self.action == 3:
            return '✋🏻'

    async def won(self):
        self.round_win = True
        self.rounds_won += 1
        self.opponent.round_win = False

    async def get_opponent(self):
        self.opponent = [p for p in self.current_battle.players if self.user_id != p.user_id][0]

    async def ask_revenge(self):
        self.revenge = True
        await self.clean_for_revenge()
        await self._update_balance()
        if self.opponent.revenge:
            self.current_battle.revenge = True
            return await self.current_battle.send_deposits()
        await self.bot.edit_message_text(chat_id=self.user_id, message_id=self.game_msg_id,
                                         text='Ожидаем ответ противника')

    async def clean_for_revenge(self):
        await self.state.set_state(BotStates.IN_BATTLE)
        self.action = None
        self.rounds_won = 0
        self.round_win = None
        self.bet = '❔'
        self.emoji_bet = ''
        self.rounds_to_win = '❔'

    async def _update_balance(self):
        from models import User
        user = User.get_by_id(self.user_id)
        self.balance = user.balance
        self.points = user.points
