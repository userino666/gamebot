import utils
from config import node, bot, CC_CONFIRMATIONS

from models import User, Transaction


def gnu_last_block(current_block):
    with open('last_block', 'r+') as f:
        last_block = int(f.read())
        f.seek(0)
        f.truncate()
        f.write(str(current_block))
    return last_block


async def query_transactions():
    current_block = node.getblockcount()
    last_block = gnu_last_block(current_block)
    block_hash = node.getblockhash(last_block)
    transactions = node.listsinceblock(block_hash)['transactions']
    print(transactions)
    for tx in transactions:
        print(tx)
        if tx['category'] not in ('receive', 'generate', 'immature'):
            continue

        await process_deposite_transaction(tx)


    # for tx in Transaction.objects.filter(processed=False, currency=currency):
    #     query_transaction(ticker, tx.txid)


async def process_deposite_transaction(txdict):
    if txdict['category'] not in ('receive', 'generate', 'immature'):
        return

    try:
        user = User.select().where(User.btc_wallet == txdict['address'])
    except User.DoesNotExist:
        return

    rub_amount = utils.btc_to_rub(amount=txdict['amount'])
    tx, created = Transaction.get_or_create(user=user, type='refill', payment_system='btc', payment_id=txdict['txid'],
                                            amount=rub_amount)

    if tx.is_completed:
        return

    if created:
        # НОВАЯ ТРАНЗАКЦИЯ
        if txdict['confirmations'] >= CC_CONFIRMATIONS and txdict['category'] != 'immature':
            tx.processed = True
            user.increase_balance(rub_amount)
            # начислить деньги на счет
    else:
        # НЕ ПОДТВЕРЖДЕННАЯ ТРАНЗАКЦИЯ
        if txdict['confirmations'] >= CC_CONFIRMATIONS and txdict['category'] != 'immature':
            tx.processed = True
            user.increase_balance(rub_amount)
            # начислить деньги на счет
    await bot.send_message(user.id, f'Ваш баланс пополнен *{rub_amount}* рублей!')
    tx.save()
