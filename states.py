from aiogram.dispatcher.filters.state import StatesGroup, State
# from aiogram.utils.helper import Helper, HelperMode, State, Item


class BotStates(StatesGroup):
    # mode = HelperMode.snake_case

    CHOOSE_NAME = State()
    START_INSTRUCTION = State()
    CHOOSE_OPTIONS = State()
    SEARCH_BATTLE = State()
    IN_BATTLE = State()
    ASK_REVENGE = State()
    BATTLE_END = State()
    CHANGE_NAME = State()
    WITHDRAW_WALLET = State()
    WITHDRAW_AMOUNT = State()
    CONFIRM_WITHDRAWAL = State()
    # STATE_5 = State()
