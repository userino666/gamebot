import utils
from models import User


def auth(func):
    async def wrapped(msg, state, call_id=None):
        try:
            user_id = msg.chat.id
        except AttributeError:
            call_id = msg.id
            user_id = msg.from_user.id

        try:
            user = User.get_by_id(user_id)
        except User.DoesNotExist:
            return await utils.user_not_found_notify(user_id, call_id)
        return await func(msg, state, user)
    return wrapped


def get_user_or_auth(func):
    async def wrapped(call, state):
        data = await state.get_data()
        user = data.get('user', None)
        if user is None:
            try:
                user = User.get_by_id(call.from_user.id)
            except User.DoesNotExist:
                return await utils.user_not_found_notify(call.from_user.id, call_id=call.id)
            await state.set_data({'user': user})
        return await func(call, state, user=user)
    return wrapped